# python

**Install Pyenv -** pyenv lets you easily switch between multiple versions of Python

```bash
sudo apt update
sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev libffi-dev liblzma-dev python3-openssl git

curl https://pyenv.run | bash

pyenv --version

pyenv install 3.6.8



```

### Python venv

```powershell
# on windows (powershell)
python -m venv .venv
.\.venv\Scripts\Activate.ps1
```
