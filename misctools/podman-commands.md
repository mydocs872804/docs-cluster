# Podman commands

```bash
podman machine init

podman machine start
```

```bash
podman run -v $(pwd):$(pwd) -w $(pwd) -itr python:3.6.8 bash

podman images
podman ps -a

podman rm --help

podman rm -f -a # remove all containers

podman exec -it reverent_gates bash
```

