# WSL Commands

```sh
wsl --install

wsl -l -v

wsl -d Ubuntu

wsl --terminate Ubuntu

wsl --shutdown

wsl --terminate Ubuntu

wsl --unregister Ubuntu # delete a specific distribution
```

