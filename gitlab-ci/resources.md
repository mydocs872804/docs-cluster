---
description: gitlab - flask application, docker and gitlab CI
---

# resources

{% embed url="https://gitlab.com/mydevops9285905/gitlab-ci/gitlab-flask-pytest-ci" %}

{% embed url="https://gitlab.com/arjun.avagadda/python-sandbox/-/blob/main/.gitlab-ci.yml" %}
