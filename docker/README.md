# Docker

### docker install

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```
```bash
sudo service docker start
docker info
sudo usermod -a -G docker "user" ( to avoid using sudo)
```

{% embed url="https://github.com/docker/docker-install" %}

#### direct execute (aws user script)

```bash
curl https://get.docker.com/ | bash
```

<table data-view="cards"><thead><tr><th></th><th></th><th></th></tr></thead><tbody><tr><td></td><td>docker image</td><td></td></tr><tr><td></td><td>docker container</td><td><ul><li>running image</li></ul></td></tr><tr><td>docker hub<br>- repo</td><td></td><td></td></tr></tbody></table>

* **Name space and cgroups** --> uses these 2 concepts to differentiate the containers and isolate them
