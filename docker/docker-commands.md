# Docker notes

### docker containers

```sh
Set-Service ssh-agent 
StartupType Automatic
Start-Service ssh-agent
Get-Service ssh-agent
```

# Docker commands

| command             | desc                                                                                                                                                        |
| ------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| docker search       | <p><code>docker search &#x3C;docker hub-image></code><br>- Search the Docker Hub for images</p>                                                             |
| docker ps           | <p>list containers<br>- <code>docker ps -a</code> >> show all containers (default shows only running)</p>                                                   |
| docker stop         | <p><code>docker stop &#x3C;id></code> <br><code></code>- stops the containers</p>                                                                           |
| docker run          | <p>-d, Run container in background and print container ID<br><br><code>docker run hello-world</code></p>                                                    |
| docker rmi          | remove image                                                                                                                                                |
| docker rm           | <p>remove container<br>-f -> Force the removal of a running container</p>                                                                                   |
| docker logs         | docker logs \<contid>                                                                                                                                       |
| docker system prune | <p><strong>removes</strong> all stopped containers <br>all networks not used by at least one container <br>all dangling images all dangling build cache</p> |

