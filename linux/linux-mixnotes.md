---
description: linux and pwsh reference commands and snippets
---

## cron
```sh
*/30 * * * * source /home/bin;del.sh  > /tmp/ArcDeletion_`date +\%H`  2>&1
```

## YOUTUBE API
```sh
curl 'https://youtube.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&pageToken=EAAaBlBUOkNHUQ&playlistId=PL-osiE80TeTjijkbfVyTiXJA-UTHn6WwU&key=<>' | jq '.items[].snippet.title'
```
```sh
#!/bin/bash

TENANCY="$1" 
KEYFILE="$2"

USER_OCID=$(oci --profile="$TENANCY" --query="data[?name == 'jack@oracle.com'].id" iam user list --all | grep ocid1.user.oc1 | sed -e 's/"//g' -e 's/,//' -e 's/ //g')

oci --profile="$TENANCY" iam user api-key upload --user-id="$USER_OCID" --key-file="$KEYFILE"
```


```sh
$ cat iam-user-password-reset.sh 
#!/bin/bash

TENANCY="$1" 
USERNAME="$2"

USER_OCID=$(oci --profile="$TENANCY" --query="data[?name == '$USERNAME'].id" iam user list --all | grep ocid1.user.oc1 | sed -e 's/"//g' -e 's/,//' -e 's/ //g')

if [ "X${USER_OCID}" = "X" ]; then
	echo "Unable to locate user $USERNAME in $TENANCY"
	exit
fi

TEMP_PASS=$(oci --profile="$TENANCY" iam user ui-password create-or-reset  --user-id="$USER_OCID" | grep password | awk '{print $2}' | sed -e 's/"//g' -e 's/,//' -e 's/ //g')

echo "${TENANCY}:${USERNAME}:${TEMP_PASS}" | tee -a iam-passwd.txt
```

```sh
#!/bin/bash

while IFS= read -r line; do
    echo "Text read from file: $line"
	TENANCY="$1"
done < file
USERNAME="$2"
echo "$TENANCY,$USERNAME"
```

```sh
cat mytenacylist | sed -e 's/\[//' -e  's/\]//' | tee -a myten

cat config | grep "\[[a-zA-Z0-9]*\]" | tee -a newten

grep "\[[a-zA-Z0-9]*\]" | cut -d "[" -f 2 | cut -d "]" -f 1 

head ansiblehost | grep filshy | sed -e 's/^[[:space:]]*//'
```
-
*gives last string*

```sh
awk -F/ ‘{print $NF}’ -- /home/arj
```

```sh
awk 'FNR==NR{array[$0];next}!($0 in array)' <file1> <file2> # difference
```

```sh
scp arjun@testlane:/tmp/config  /tmp

[root@main ~]# getent passwd arjun

find Pictures/tecmint/ -name "*.png" -type f -print0 | xargs -0 tar -cvzf images.tar.gz

xrags -L1 ansible -m ping


tr -d '\r' < file.csv > file2.csv  # removing ^M from a csv file

Type :3,5d  # vi delete range lines

:%d # vi delete all lines

nohup bash dbmain.sh &

nohup bash list_host.sh &> win_nonrfc.out &

nohup bash -c 'for i in `cat win_tmp`;do ansible -m win_ping $i;done' > win_tmp.txt &
```

```sh
[root@omcscabzffscfa os_mgmt]# bash -x flakes.sh /home/arjun/backuplist
+ '[' 1 -eq 0 ']'
+ flakes=/home/arjun/backuplist
+ logfile=
++ echo /home/arjun/backuplist
++ awk -F/ '{print $NF}'
+ logfile=backuplist
+ '[' backuplist == '' ']'
+ read -r Tenancy
+ echo '###########arjun##########'
###########alya01##########
++ oci --profile=arjuntest '--query=data[?name == '\''backup'\''].id' iam user list --all
++ jq -r '.[]'
+ user_ocid=ocid1.user.oc1..aaaaaaaahujuib5dojl4ev43kxlkylrutjggujarusheibr64hsakb4q
```

### awk script
```sh
$ awk -F"|" '{ a[$1 OFS $2]+=$3; b[$1 OFS $2]+=$4; c[$1 OFS $2]+=$5 }
  END {
    for (i in a) {
      print i, a[i], b[i], c[i];
    }
  }
' OFS=\| file.csv

cat sandboxcsv.csv | awk -F"|" '
  { a[$2] += $2 }
  END {
    for (i in a) {
      printf "%s|%s\n", i, a[i];
    }
  }
'
```

```sh
$ ls -A1  # only file names

grep -rni "string" * # recursively search entire directory files for a string

grep -Ril "text-to-find-here" /     

grep -w # exact match

cp -p -R 

vim 
2yy # copy
p # for paste
hostname -i

```

### grep csv last line iterate

```sh
while IFS= read -r line; do
  echo "$line"
done < <(grep "" file)

cat newhostlist.csv | grep -v oracleoutsourcing | awk -F, '{print $1}' | sort | uniq

csvstack *.csv  > out.csv  # merge multiple files
```

# powershell
```pwsh
Import-Csv .\test.csv | Format-Table -AutoSize
```

## pwsh git branch name

```pwsh
function parse_git_branch () {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

RED="\[\033[01;31m\]"
YELLOW="\[\033[01;33m\]"
GREEN="\[\033[01;32m\]"
BLUE="\[\033[01;34m\]"
NO_COLOR="\[\033[00m\]"

- without host
#PS1="$GREEN\u$NO_COLOR:$BLUE\w$YELLOW\$(parse_git_branch)$NO_COLOR\$ "
- with host
PS1="$GREEN\u@\h$NO_COLOR:$BLUE\w$YELLOW\$(parse_git_branch)$NO_COLOR\$ "
```

## ssh pem key

ssh-keygen -f id_rsa -e -m pem

clear the ssh keys : ssh-add -D

## shells
chsh -s /bin/bash

cat /etc/shells

### zsh show run time 

```sh
function preexec() {
  timer=$(($(date +%s%0N)/1000000))
}

function precmd() {
  if [ $timer ]; then
    now=$(($(date +%s%0N)/1000000))
    elapsed=$(($now-$timer))

    export RPROMPT="%F{cyan}${elapsed}ms %{$reset_color%}"
    unset timer
  fi
}
```

### run time in linux

```sh
time <command>  # measures the run time
```

### run time in windows
```pwsh
Measure-Command { python .\hello.py }
```

## trim leading and trailing whitespace 

```sh
awk '{$1=$1};1'
```


## append contents of multiple files into one file

```sh
cat 1.txt 2.txt 3.txt > 0.txt
```

## random commands

### pwsh

```pwsh
powershell version : $PSVersionTable
# pwsh tail

In Powershell you can use Get-Content with the -Wait flag:
Get-Content filename.log -Wait
You can shorten Get-Content to gc

Get-Content .\20230904123014.log -Wait -Tail 20
```

### linux



```sh
# change shell from zsh to bash

chsh -s $(which bash) {{user}}

# export environment vars in shell script

https://stackoverflow.com/questions/63291606/set-env-var-in-bash-script

export ENV_1=firstparam
export ENV_2=secondparam

# for loop
for i in `cat serach`;do grep $i config ;done
## for in one line
for i in `cat /home/aavagadd/ping/linux_28aug/linuxtenancy`; do ansible-playbook -v zpingsand.yml -i /srv/os_mgmt/inventory/$i | tee -a /home/aavagadd/ping/linux_28aug/logs/$i.log; done

for VARIABLE in file1 file2 file3

https://www.cyberciti.biz/faq/linux-unix-bash-for-loop-one-line-command/

# mpstat

mpstat 2 5 #Display five reports of global statistics among all processors at two second intervals.
              
## RAM

lshw --> RAM info

Curl -v telnet://<hostname>:<port>

grep -r dataset --exclude-dir={inventory,patch_playbooks,old_bkp_IMPORTANT} .

## find config

find /home/*/.oci/config -type f -exec grep -l "lankan" {} \;


## find command ( delete older files )

find . -type f -mtime "+$(( ( $(date '+%s') - $(date -d '2 months ago' '+%s') ) / 86400 ))"

find /tmp/*.log -type f \
-mtime "+$(( ( $(date '+%s') - $(date -d '3 months ago' '+%s') ) / 86400 ))" -delete

## word count without spaces

grep -v ^$ /tmp/ten | wc -l

# date

date '+%Y%m%d'

## inventory - know the count

awk -F ',' '{print $1}' raw_20231111020006.csv | sort | uniq | tee -a /tmp/old

diff -y /tmp/nov14old /tmp/nov14new
```
-
[remove-fullpath-from-terminal](https://askubuntu.com/questions/232086/remove-full-path-from-terminal)

-
default nginx location : /usr/share/nginx/html

-
```sh
curl -s # silent
curl -v # verbose
```