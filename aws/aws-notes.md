---
description: aws points from various resources
---

# aws budgets and alerts

1. we can set cloudwatch billing alerts
2. setup budgets ( free tier - only2)
<figure><img src="../.gitbook/assets/aws-zerospend-budget.png" alt=""><figcaption><p>aws-zerospend-budget</p></figcaption></figure>

3. billing with freeform tags
- cost allocation tags
4. cost explorer
5. Aws credits
6. aws calculator
- [aws-calculator](https://calculator.aws/#/)
7. aws free tier


# security

1. aws organizations
2. aws cloudtrail - api calls
3. aws usergroups , roles, policies

# aws arch diagrams

[aws-arch-diagrams](https://aws.amazon.com/architecture/icons/)



