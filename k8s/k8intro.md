---
description: basic commands and misc notes
---

# k8Intro

## K8Intro

Custom Resource Definitions - CRD resource group sanity testing

> kubeconfig file - /root/.kube/config

[lens](https://k8slens.dev/)

k8s manifest

### cordon and uncordon

drain cordon and uncordon --> for maintance ( Ready,SchedulingDisabled)

```sh
ku drain <node> --ignore-daemonsets
ku uncordon <node>
```

### Types of Deployments

> Deploying app with single pod is a SPOF(single point of failure)

1. Deployment (Stateless Applications)- recommended for **stateless** web & app pods , no dependency on the POD names and we can create n number of replicas.
2. Daemonset (Monitoring & log collection) - run pod per host(node) ex., monitoring(node explorter),logs collection(ELK,Filebeat)
3. StatefulSet (stateful applications)- starts pods in sequence,pod names should not change,same PV must be attached.

### k8 output templating

[go-template](https://blog.dkwr.de/development/kubernetes-go-templates/)

[redhat-gotemplate](https://cloud.redhat.com/blog/customizing-oc-output-with-go-templates)

## kubernetes commands

```sh
kubectl cluster-info dump  # gives all the info about the cluser(more verbose)
kubectl get nodes

kubectl get pods -n kube-system # get api server,controller manager etcd in kube-system namespace

kubectl get nodes --no-headers
kubectl get nodes -o wide

kubectl config set-context --current --namespace=kube-system # set namespace context

New-Alias -Name "ku" kubectl  # set alias in powershell

New-Alias -Name "docker" podman

kubectl create deployment deploy1 --image=arjun207/firstimage:latest --dry-run=client -o yaml

kubectl create deployment deploy1 --image=arjun207/firstimage:latest --dry-run=server -o yaml
[stackq](https://stackoverflow.com/questions/64279343/kubectl-dry-run-is-deprecated-and-can-be-replaced-with-dry-run-client)

ku api-resources # list all the api repources that can be created

ku explain deployments # 

ku explain deployments.spec.template.spec

ku describe po deploy1-6685796c64-zgcgq

ku logs deploy1-6685796c64-zgcgq

ku logs -v=8 deploy1-5566fc4bff-4jbs7 -f

kubectl get pods -A -o=custom-columns='DATA:spec.containers[*].image' -n default

```

*

**dry-run** --> Must be "none", "server", or "client". If client strategy, only print the object that would be sent, without sending it. If server strategy, submit server-side request without persisting the resource.
