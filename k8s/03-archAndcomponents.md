---
description: architecture and k8 components
---

## k8 architecture

<figure><img src="../.gitbook/assets/Architecture.png" alt=""><figcaption><p>K8 architecture</p></figcaption></figure>

# 4. k8 components

<figure><img src="../.gitbook/assets/k8-components.png" alt=""><figcaption><p>K8 componets</p></figcaption></figure>

pod can have 1 or more containers (main and sidecar containers)

container > POD > replicaset > deployment   --> service(SVC)
