---
description: services and ingress controllers
---

# 6. Services

## commands used

```sh
kubectl create deployment app1 --image=nginx:latest --replicas 3
kubectl expose deployment app1 --port 80 #service/app1 exposed
kubectl get ep app1 # get end points

kubectl port-forward deployments/app1 80:80
kubectl describe svc/app1

kubectl expose deployment app1 --port 80

root@controlplane:~$ k describe svc app1 # end points
Endpoints:         10.244.1.2:80

kubectl expose deployment app1 --port 80 --target-port 8888 --type NodePort -o yaml --dry-run # node port
```

- svc can be used with pod,rs,deploy,daemonset
- when you create a svc check the end points on the svc
- port forward can be done for deploy,svc,pods


## Expose application k8 resources
1. ClusterIP - only internal to the cluster (database), ones which are not exposed to internet
2. NodePort - exposed to internet, this also mostly used for testing
3. LoadBalancer
    - if we have 1 or 2 services then we can go for loadbalancer, for many go with ingress controller.
    - loadbalancers are limited to a region
4. Headless Service (Datebase with statefullset)
5. External name


- end points in svc describe
-
clusterip nodeport
<figure><img src="../.gitbook/assets/clusterip-nodeport.png" alt=""><figcaption><p>ClusteripAndNodeport</p></figcaption></figure>


# 7.Ingress controller

[pending]