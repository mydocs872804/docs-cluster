---
description: k8 cluster creation and external resources,tools.
---

# installation

## k8 gitlab repo

[k8-repo-manifest](https://gitlab.com/mydevops9285905/k8-running-notes)

### different methods to install k8s

1. kind is a tool for running local Kubernetes clusters using Docker container “nodes”. kind was primarily designed for testing Kubernetes itself, but may be used for local development or CI.
2. [minikube](https://minikube.sigs.k8s.io/docs/start/)
3. [k3s](https://k3s.io/)

### production grade

* [KOPS](https://kubernetes.io/docs/setup/production-environment/tools/kops/)
* [RKE](https://rke.docs.rancher.com/)

## KOPS installation

1. DNS zone - route53
2. ubuntu 20.04 on custom VPC
3. install aws cli [aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
4. install kubectl [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)
5. kops install -

```sh
(root@ip-10-19-0-103:/home/ubuntu# kops version
Client version: 1.28.1 (git-v1.28.1)
```

6. genrate ssh keys
7. create s3 bucket
8. create user and setup aws cli for KOPS
9. set environment variables

```sh
export NAME=arjunavagadda.live
export KOPS_STATE_STORE=s3://k8s-nov17
export AWS_REGION=ap-south-1
export CLUSTER_NAME=arjunavagadda.live
```

```sh
kops create cluster # https://kops.sigs.k8s.io/getting_started/aws/
```

```sh
kops create cluster --name=arjunavagadda.live --state=s3://k8s-nov17 \
--zones=ap-south-1a,ap-south-1b,ap-south-1c --node-count=3 --control-plane-count=1 \
--node-size=t2.micro --control-plane-size=t2.micro --control-plane-zones=ap-south-1a \
--control-plane-volume-size 10 --node-volume-size 10 --ssh-public-key ~/.ssh/id_rsa.pub \
--dns-zone=arjunavagadda.live --networking calico --dry-run --output yaml > k8scluster.yaml
```

**stores config in s3 bucket**

```sh
kops create -f k8scluster.yml

kops update cluster --name arjunavagadda.live --yes --admin

kops validate cluster --wait 10m

kops delete cluster --name arjunavagadda.live --yes

kops delete -f cluster.yml --yes
```

### install docker

```sh
apt install docker.io
```

### install kind

```sh
[ $(uname -m) = x86_64 ] && curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.20.0/kind-linux-amd64

chmod +x ./kind
sudo mv ./kind /usr/local/bin/kind
```

### kind cluster commands

```sh
kind create cluster # create cluster

kind delete cluster

kind get clusters # get cluster
```

#### kind cluster with multiple nodes

[kind-multinode-cluster](https://kind.sigs.k8s.io/docs/user/quick-start/#configuring-your-kind-cluster)

```sh
kind create cluster --config kind-example-config.yaml
```

```yaml
# three node (two workers) cluster config
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
- role: worker
```

### kubectl install

```sh
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x kubectl
mv kubectl /usr/local/bin/

```

### K8 Auto completion

```sh
alias ku=kubectl
source <(kubectl completion bash) # set up autocomplete in bash into the current shell, bash-completion package should be installed first.
echo "source <(kubectl completion bash)" >> ~/.bashrc # add autocomplete permanently to your bash shell.
complete -o default -F __start_kubectl ku
```

[k8-autocompletion](https://komodor.com/learn/kubectl-autocomplete-enabling-and-using-in-bash-zsh-and-powershell/)

* [k8-krew](https://krew.sigs.k8s.io/docs/user-guide/setup/install/)

### kubenx and kubectx

[git-kubectx](https://github.com/ahmetb/kubectx)

### ku-neat

https://github.com/itaysk/kubectl-neat

### kubernetes release

* [OKE](https://docs.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengaboutk8sversions.htm)
* [EKS](https://docs.aws.amazon.com/eks/latest/userguide/kubernetes-versions.html)
* [K8](https://kubernetes.io/releases/)

### k8 playgrounds

[killercoda](https://killercoda.com/playgrounds/scenario/kubernetes)
