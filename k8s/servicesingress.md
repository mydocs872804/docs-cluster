---
description: services
---

# ServicesIngress

## commands used

```sh
kubectl create deployment app1 --image=nginx:latest --replicas 3
kubectl expose deployment app1 --port 80 #service/app1 exposed
kubectl get ep app1 # get end points

kubectl port-forward deployments/app1 80:80
kubectl describe svc/app1
```

* svc can be used with pod,rs,deploy,daemonset
* when you create a svc check the end points on the svc
* port forward can be done for deploy,svc,pods
