---
description: deployment port forwarding replicaset and rolling updates
---

# DeployReplicaPortfwd

## 4. Deploy

```sh
kubectl scale deployment --replicas 6 deploy1 # imperative format

kubectl get events -A # lists all the cluster events

kubectl api-resources # list all the api resources (pod,replica) that can be created inside k8

kubectl api-resources | wc -l # get the count of api reources

kubectl run app1 --image=nginx:latest # run a pod

kubectl run --help # u have a lot more options 

kubectl -v 8 logs app1 -f # tail logs of pod at verbose 8

```

### port forwarding

```sh
ku port-forward pod/app1 80:80 # forward port

kubectl port-forward pods/app1 --address 0.0.0.0 8000:80

kubectl exec -it app1 -- bash # get inside the container "--" is given so whatever after it will execute inside the container

Enable metrics from Lens and run the following.
kubectl port-forward services/prometheus --address 0.0.0.0 9090:80 -n lens-metrics

```

### replica set

```sh
kubectl create deploy --help

kubectl describe rs/frontend # get replica sets
```

```sh
kubectl get deployments -o  go-template='{{.apiVersion}}{{"\n"}}' # gives version v1

kubectl get deployments -o  go-template-file= # go template file to filter

kubectl get deployments --all-namespaces -o go-template-file=./deployments-limits.gotemplate

kubectl get po --help

kubectl get po --show-labels

kubectl delete pod frontend-8rwcq --force --grace-period=0 # forceful shutdown

# Update pod 'foo' with the label 'status' and the value 'unhealthy', overwriting any existing value
kubectl label --overwrite pods foo status=unhealthy

# Edit the service named 'registry'
kubectl edit svc/registry

# Edit the deployment 'mydeployment' in YAML and save the modified config in its annotation
kubectl edit deployment/mydeployment -o yaml --save-config


```

> k8 every thing happens on _labels_

* replica sets will not update the image if you change image/env then you should manual reboot or destroy and create the containers.

## 5. k8 deployment and rolling updates

> if the application do not have any backward compatability and all together if we create new servers then replica set is used

```sh
kubectl create deployment app1 --image nginx:latest --replicas 3 --dry-run=client -o yaml
kubectl explain deployment.spec.strategy.rollingUpdate.maxSurge
kubectl get rs # get replicasets (rs-replicasets)

kubectl rollout status deploy app1 # check the status of the rolling update deployment

kubectl rollout resume deploy app1 # pause the deploy
kubectl rollout pause deploy app1 # resume
kubectl rollout history deploy/app1 # View rollout history
kubectl rollout undo deploy/app1  # Undo a previous rollout
```

### rolling update

* maxSurge : maximun number of pods that to be added above the desired
* maxUnavailable : max number of pods that can be unavailable during the update
* minReadySeconds : interval
