---
description: reference - oci cli commands
---

# oci cli

### oci config file permissions in windows

oci setup repair-file-permissions --file C:\Users\aavagadd\.oci\config

### OCI - IAM

```bash
oci iam compartment list --all

oci --profile prod_profile iam user list --limit 5

oci --profile prod_profile --query "data[?name == 'test@email.com'].id" iam user list --all

oci --profile prod_profile --query "data[?name == 'test'].id" iam user list --all

# auth token

oci iam auth-token delete --force --auth-token-id "ocid1.credential.oc1....." --user-id "ocid1.user.oc1.."

oci --profile prod_profile iam auth-token list --user-id "ocid1.user.oc1.."

oci --profile prod_profile iam auth-token create --user-id "ocid1.user.oc1.." --description="test"

oci --profile prod_profile --query "data[?name == 'test']" iam user list --all | grep ocid1.user.oc1 | sed -e 's/"//g' -e 's/,//' -e 's/ //g'

oci --profile prod_profile --query "data [*].{id:id, OCID:\"user-id\"}" iam auth-token list --user-id="ocid1.user.oc1.."

oci --profile prod_profile --query "data[?name == 'test'].\"compartment-id\"" iam user list --all

oci --profile prod_profile --query "data[?name == 'test'].id" iam user list --all --raw-output | jq -r '.[]'

```

### OCI - network

```bash
oci --profile prod_profile network subnet list --compartment-id ocid1.compartment.oc1... --vcn-id ocid1.vcn.oc1.phx...

oci --profile prod_profile network subnet get --subnet-id ocid1.subnet.oc1.phx.......

```

### OCI - compute

```bash

oci --profile arjun compute instance action --action STOP --instance-id=ocid1.instance.oc1.iad...

oci --profile test search resource free-text-search --text RUNNING --query "data[].{Name:\"display-name\",state:\"lifecycle-state\",id:id}"


oci --profile arjun search resource structured-search --query-text "QUERY instance resources where lifeCycleState='STOPPED'"  --query 'data.items[*].identifier' --raw-output

oci --profile test search resource structured-search --query-text "QUERY instance resources where lifeCycleState='RUNNING'"  --query 'data.items[*].identifier' --raw-output | jq -r '.[]'

cat jsonfetch | jq '.data[]."display-name"'

```

### OCI - DB and VM cluster

```bash

oci --profile test db cloud-exa-infra list -c ocid1.compartment.oc1..aaaaaa.. --all

oci --profile test db autonomous-exadata-infrastructure list -c ocid1.compartment.oc1..aaaaaaa... --all

oci --profile test db node list -c ocid1.compartment.oc1..aaaaaaa... --db-system-id ocid1.dbsystem.oc1.iad...

oci --profile test db database list -c ocid1.compartment.oc1..aaaaaaa....

oci --profile test db cloud-vm-cluster list -c ocid1.compartment.oc1..aaaaaaa... --all

oci --profile test db cloud-exa-infra list -c ocid1.compartment.oc1..aaaaaaa...

```

### OCI - Cloudguard

```bash

oci --profile test cloud-guard problem list --all -c ocid1.compartment.oc1..aaaaaaaacp.. --region me-jeddah-1

oci --profile test cloud-guard problem list --all -c ocid1.compartment.oc1..aaaaaaaaybu5y2... 

oci cloud-guard security-score-aggregation request-security-scores [OPTIONS]

--query="data.items[].{detid:\"detector-id\", risklevel:\"risk-level\"}"

--query="data [*].{id:id, OCID:\"user-id\"}" 

oci --profile test cloud-guard problem list --all -c "ocid1.compartment.oc1..aaaaaaaarbpupk2seip" | jq -r '.data.items[]."detector-id" , .data.items[]."detector-rule-id" , .data.items[]."resource-type" ,  .data.items[]."risk-level"' | paste -sd, -

oci cloud-guard problem list-problem-entities --problem-id ocid1.cloudguardproblem.oc1.phx.amaaaaaahcgm2jc...

```
