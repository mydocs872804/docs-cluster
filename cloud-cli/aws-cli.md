---
description: aws cli commands reference
---

# aws-cli

## installtion

[getting-started-install](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

```sh
aws --cli-auto-prompt # aws cli auto prompt
```

### aws ec2

```sh
aws ec2 describe-instances

aws ec2 stop-instances --instance-ids <>

aws ec2 start-instances --instance-ids <>

aws ec2 describe-instances | jq ".Reservations[].Instances[].State"

aws ec2 describe-instances --query 'Reservations[].Instances[].[InstanceId,InstanceType,PublicIpAddress,Tags[?Key==`Name`]| [0].Value]' --output table

aws ec2 describe-availability-zones --region ap-south-1
```

### aws s3

```bash
aws s3 help

aws s3 ls help

aws s3 ls

aws s3 ls s3://test-bkt-fap

```

### aws-sts

```sh
aws sts get-caller-identity
```
