---
description: ansible commands and running notes
---

# ansible

stapp01 ansible\_host=172.16.238.10 ansible\_user=tony ansible\_password=Ir0nM@n stapp02 ansible\_host=172.16.238.11 ansible\_user=steve ansible\_password=Am3ric@ stapp03 ansible\_host=172.16.238.12 ansible\_user=banner ansible\_password=BigGr33n

## ansible test - hello world

```yaml
---
  - name: ping Playbook
    hosts: localhost
    become: yes
    tasks:
      - name: ping host
        ansible.builtin.ping:

---
  - name: Playbook
    hosts: all
    #become: yes
    tasks:
      - name: 'What is my number'
  delegate_to: 'localhost'
  when: >
    local_username is undefined
  shell: >
    cat /proc/$$/loginuid
  register: 'loginuid'
  changed_when: 'false'
- debug:
    var: loginuid

```

execlude multiple groups

```sh
ansible-playbook sample.yaml -l !OC7:!OC8
```

list ansible hosts

```sh
ansible --list-hosts hosts3 
```

start at task

```sh
ansible-playbook site.yml --limit='' --start-at-task='configure_rsyslog'
```

*

```sh
ansible all -i inventory -m yum -a "name=abrt* state=absent"
```

*

```sh
ansible-playbook playbook.yml --start-at-task="install packages"
```

Perform task: configure ssh (y/n/c): Answering “y” will execute the task, answering “n” will skip the task, and answering “c” will continue executing all the remaining tasks without asking.

*

**ansible supress python and cryptography warnings**

```sh
ANSIBLE_DEPRECATION_WARNINGS=false PYTHONWARNINGS=ignore::UserWarning ANSIBLE_PYTHON_INTERPRETER=auto_silent ansible -m ping <>

export ANSIBLE_DEPRECATION_WARNINGS=false
export PYTHONWARNINGS=ignore::UserWarning
```

*

ansible gather facts magic variable

```sh
ansible <hostname> -m ansible.builtin.setup
```

## Ansible flags

\--step

\--check https://docs.ansible.com/ansible/latest/user\_guide/playbooks\_checkmode.html

\--limit

https://docs.ansible.com/ansible/2.4/playbooks\_startnstep.html

*

**get group names**

ansible albehd1v-win-6.netxkpbfs.vcnirdoi.oraclevcn.com -m debug -a "var=group\_names"

## targeting nodes and patterns

```sh
ansible -m ping 'host_region_*win*'

'*_rhel_gen:*_rhel:*_odc:*_win'

ansible '*_rhel_gen:*_rhel:*_odc:*_win' -i inventory/test1 -m ping

ansible '!*_win*' -i inventory/test1 -m ping
```

## ansible logs

```sh
ANSIBLE_LOG_PATH=/tmp/ansible_$(date "+%Y%m%d%H%M%S").log ansible-playbook myplabook.yml
```

https://stackoverflow.com/questions/39455532/how-to-append-date-and-timestamp-in-the-ansible-log-file

## callbackplugin for logs

https://docs.ansible.com/ansible/latest/plugins/callback.html
